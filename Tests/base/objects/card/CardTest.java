package base.objects.card;

import PaD.PlancheADessin;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CardTest {
    final Card card1 = new Card(CardValue.QUEEN, CardColor.CLUBS);
    final Card card2 = new Card(CardValue.SIX, CardColor.CLUBS);
    final Card card3 = new Card(CardValue.QUEEN, CardColor.CLUBS);
    final Card card4 = new Card(CardValue.QUEEN, CardColor.SPADES);
    final PlancheADessin canvas = new PlancheADessin(1000, 1000);

    @BeforeEach
    void setUp() {
        canvas.clear();
    }

    @Test
    void compareTo() {
        assertTrue(card1.compareTo(card2) > 0);
        assertTrue(card3.compareTo(card4) < 0);
    }


    @Test
    void draw() throws InterruptedException {
        card1.draw(canvas, 100, 100);
        Thread.sleep(2000);

    }

    @Test
    void getColor() {
        assertEquals(CardColor.CLUBS, card1.getColor());
    }

    @Test
    void getValue() {
        assertEquals(CardValue.QUEEN, card1.getValue());
    }

    @Test
    void ToString() {
        assertEquals("[QUEEN(10),CLUBS]",card1.toString());
    }

    @Test
    void Equals() {
        assertEquals(card1,card1);
    }


    @Test
    void flipCard() throws InterruptedException {
        card1.draw(canvas, 100, 100);
        Thread.sleep(2000);
        card1.flipCard();
        card1.draw(canvas,100,100);
        Thread.sleep(2000);
    }
}