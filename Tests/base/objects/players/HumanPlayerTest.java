package base.objects.players;

import base.game.Game;
import org.junit.jupiter.api.Test;


class HumanPlayerTest {

    @Test
    void playTurn() {
        Player player = new HumanPlayer("player1");
        Game game= new Game(player);
        player.playTurn(game);
    }

}