package base.objects.players;

import static org.junit.jupiter.api.Assertions.*;

import PaD.PlancheADessin;
import base.game.Game;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class PlayerTest {
    Game game;
    Player player;

    @BeforeEach
     void setUp() {
        player = new Player() {
            @Override
            public void playTurn(Game game) {

            }
        };
        game = new Game(player);

    }

    @Test
     void takeCard() {
        player.takeCard(game);
        assertTrue(player.getCards().size() > 0);
    }

    @Test
    void draw() throws InterruptedException {
        PlancheADessin canvas = new PlancheADessin(1000, 1000);
        player.takeCard(game);
        player.takeCard(game);
        player.takeCard(game);
        player.draw(canvas, 100, 100, 100);
        Thread.sleep(1000);

    }

    @Test
    void getName() {
        assertEquals("Player",player.getName());
    }

    @Test
    void getPlayerValue() {
        player.takeCard(game);
        assertTrue(player.getPlayerValue()>0);
    }



    @Test
    void isBusted() {
        player.takeCard(game);
        player.takeCard(game);
        player.takeCard(game);
        player.takeCard(game);
        player.takeCard(game);
        player.takeCard(game);
        player.takeCard(game);
        assertTrue(player.isBusted());
    }

}