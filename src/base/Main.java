package base;

import base.game.Game;
import base.objects.players.BotPlayer;
import base.objects.players.HumanPlayer;
import base.objects.players.Player;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class Main {
    public static void main(String[] args) throws InterruptedException {


        boolean inputsNotValidated = true;
        int numberBots = 0;
        int numberOfPlayers = 0;
        Random randomGenerator = new Random();

        while (inputsNotValidated) {
            try {
                String s = JOptionPane.showInputDialog(null, "Give the number of bots: ");
                numberBots = Integer.parseInt(s);
                s = JOptionPane.showInputDialog(null, "Give the number of Human players: ");
                numberOfPlayers = Integer.parseInt(s);
                inputsNotValidated = false;
            } catch (HeadlessException | NumberFormatException e) {
                e.printStackTrace();
            }

        }
        // creating game
        Game game = new Game();

        // adding game
        for (int i = 0; i < numberBots; i++) {
            game.addPlayer(new BotPlayer(Player.PlayerNames[randomGenerator.nextInt(Player.PlayerNames.length)]));
        }
        for (int i = 0; i < numberOfPlayers; i++) {
            String playerName = JOptionPane.showInputDialog(null, "Give player " + i +" name: ");
            game.addPlayer(new HumanPlayer(playerName));
        }
        boolean running = true;
        while (running) {
            game.startGame();
            int returnValue = JOptionPane.showOptionDialog(null, "Play again", null,
                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.ERROR_MESSAGE, null, new String[]{"yes", "No"}, 0);
            if (returnValue == 1) {
                running = false;
            }
        }
        System.exit(1);

    }
}
