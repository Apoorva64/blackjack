package base.game;

import PaD.PlancheADessin;
import PaD.RectanglePlein;
import base.objects.players.Dealer;
import base.objects.players.Player;
import base.objects.card.Card;
import base.objects.card.CardColor;
import base.objects.card.CardValue;
import base.objects.players.PlayerState;

import java.awt.*;
import java.util.*;

import static base.game.PaddingSizes.Y_PAD;
import static java.lang.Math.random;

/**
 * BlackJack game
 */
public class Game {
    protected final ArrayList<Card> cardDeck;
    private static final int width = 1000, height = 700;
    private final PlancheADessin canvas = new PlancheADessin(width, height);
    private final ArrayList<Player> players;
    private final Dealer dealer;
    private boolean GameStarted = false;
    public static final int FONT_SIZE = 20;
    public static final Font FONT = new Font("Helvetica", Font.BOLD, FONT_SIZE);

    public Game() {
        this.cardDeck = new ArrayList<>();
        this.players = new ArrayList<>();

        this.dealer = new Dealer();
        reset();


    }

    public Game(Player... players) {
        this();
        Collections.addAll(this.players, players);
    }


    public PlancheADessin getCanvas() {
        return canvas;
    }

    /**
     * resets the game so that we can start it again
     */
    private void reset() {
        //reset players
        for (Player player :
                this.players) {
            player.reset();
        }
        this.dealer.reset();

        // clear card deck
        this.cardDeck.clear();

        // fill card deck
        for (int i = 0; i < 2; i++) {
            for (CardColor color : CardColor.values()) {
                for (CardValue value :
                        CardValue.values()) {
                    Card card = new Card(value, color);
                    this.cardDeck.add(card);
                }
            }
        }
        // mix card deck
        mixCards();

    }


    /**
     * Gets a specific card at a given index
     *
     * @param index : index of the card in cardDeck array
     * @return: the card at index
     */
    public Card getCard(int index) {
        if (index >= 0 && index <= this.cardDeck.size() - 1) {

            return this.cardDeck.remove(index);
        }
        return null;
    }

    public Card getLastCard() {
        if (this.cardDeck.size() > 0) {
            return this.cardDeck.remove(cardDeck.size() - 1);
        } else {
            throw new IndexOutOfBoundsException("CARD DECK IS EMPTY");
        }
    }


    /**
     * Mixes cards in cardDeck by swapping 2 cards in the array 52 times
     */
    public void mixCards() {
        for (int i = 0; i <= 52; i++) {
            int elementIndex1 = (int) (random() * (this.cardDeck.size() - 1));
            int elementIndex2 = (int) (random() * (this.cardDeck.size() - 1));
            Card element1 = this.cardDeck.get(elementIndex1);
            Card element2 = this.cardDeck.get(elementIndex2);
            this.cardDeck.set(elementIndex1, element2);
            this.cardDeck.set(elementIndex2, element1);
        }
    }

    /**
     * Sorts the cards in cardDeck with their compareTo() method
     */
    public void sortCards() {
        Collections.sort(this.cardDeck);
    }

    @Override
    public String toString() {
        return "CardGame52{" +
                "cardDeck=" + cardDeck.toString() +
                '}';
    }

    /**
     * Draws the full card deck on screen
     */
    public void drawCardDeck(int x, int y, int spaceBetweenCards) {
        for (Card card :
                this.cardDeck) {
            card.draw(canvas, x, y);
            y += spaceBetweenCards;

        }
    }

    /**
     * Redraws the players and the dealer
     */
    public void redraw() {
//        this.canvas.clear();
        this.dealer.draw(canvas, (double) width / 2 - 100, Y_PAD.getValue(), 200);
        int x = 0;
        int y = Y_PAD.getValue() + height / 3;
        for (Player player :
                players) {
            player.draw(canvas, x, y, (double) width / this.players.size());
            x += (double) width / this.players.size();

        }


    }

    /**
     * Adds a player to the game
     *
     * @param player: player to be added to the game
     */
    public void addPlayer(Player player) {
        if (!this.players.contains(player)) {
            if (!GameStarted) {
                this.players.add(player);
                return;
            }
            throw new IllegalArgumentException("Cannot add player while game is running");

        }
        throw new IllegalArgumentException("Player has already been added");


    }

    public void removePlayer(Player player) {
        if (!this.players.contains(player)) {
            if (!GameStarted) {
                this.players.remove(player);
            }
            throw new IllegalArgumentException("Cannot add player while game is running");

        }
        throw new IllegalArgumentException("Player is not in the current players");

    }

    public void startGame() throws InterruptedException {
        canvas.clear();
        reset();
        //draw background
        canvas.ajouter(new RectanglePlein(0, 0, width, height, Color.GREEN));

//        draw deck
        for (Card card :
                this.cardDeck) {
            card.flipCard();
        }
        drawCardDeck((int) (width - Card.TEXTURE_DIMENSION[0]), 0, 2);


        // give cards
        GameStarted = true;
        for (Player player :
                this.players) {
            player.clear();
            player.takeCard(this);
            player.takeCard(this);
        }

        dealer.takeSingleCard(this);

        dealer.flipCards();
        redraw();

        // play game
        for (Player player :
                this.players) {
            if (!player.isBusted()) {
                player.playTurn(this);
            }
            redraw();
        }
        dealer.flipCards();
        // show all dealer's cards
        dealer.takeCard(this);
        Thread.sleep(100);
        redraw();

        // check who won the game
        for (Player player :
                players) {
            if (!player.isBusted() && (dealer.isBusted() || player.getPlayerValue() > dealer.getPlayerValue())) {
                player.setState(PlayerState.WIN);

            } else {
                player.setState(PlayerState.LOSE);
            }
        }
        redraw();
        GameStarted = false;

    }
}
