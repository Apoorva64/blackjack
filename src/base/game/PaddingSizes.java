package base.game;

/**
 * Enum containing random values to fix padding issues in PaD
 */
public enum PaddingSizes {
    X_PAD(10), Y_PAD(100), FONT_HEIGHT(14 * 2);
    private final int value;

    PaddingSizes(int x) {
        this.value = x;
    }

    public int getValue() {
        return value;
    }
}
