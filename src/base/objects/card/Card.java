package base.objects.card;

import PaD.Image;
import PaD.PlancheADessin;

/**
 * Card class
 */
public class Card implements Comparable<Card> {
    private final CardColor color;
    private final CardValue value;
    private final PaD.Image texture;
    private PaD.Image currentTexture;
    private final Image cardBack = new PaD.Image("./data/cards/" + "dos.gif");
    private CardState state = CardState.FACE;
    public static final double[] TEXTURE_DIMENSION = new double[2];


    /**
     * @param value: Value of the card
     * @param color: color of the card
     */
    public Card(CardValue value, CardColor color) {
        this.color = color;
        this.value = value;
        this.texture = new PaD.Image("./data/cards/" + this.value.getName() + "-" + this.color.getName() + ".gif");
        TEXTURE_DIMENSION[0] = cardBack.getLargeur();
        TEXTURE_DIMENSION[1] = cardBack.getHauteur();
        this.currentTexture = texture;
    }

    public CardColor getColor() {
        return color;
    }

    public CardValue getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "[" + value + "(" + value.getValue() + ")" + "," + color + ']';
    }


    /**
     * method to compare two cards
     *
     * @param other : other card to compare to
     * @return 1 if current card is superior, 0 is current card is equal, -1 if current card is inferior
     */
    public int compareTo(Card other) {
        Integer cardColor = this.getColor().ordinal();
        Integer otherColor = other.getColor().ordinal();
        int cardColorComparison = cardColor.compareTo(otherColor);
        if (cardColorComparison != 0) {
            return cardColorComparison;
        }
        return this.getValue().compareTo(other.getValue());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return color == card.color && value == card.value;
    }

    /**
     * Draws the card on a certain xy position on a certain canvas
     *
     * @param canvas: canvas to drawCardDeck on
     * @param x:      x position of the card
     * @param y:      y position of the card
     */
    public void draw(PlancheADessin canvas, double x, double y) {
        this.currentTexture.setOrig(x, y);
        canvas.ajouter(this.currentTexture);
        canvas.redraw();
    }

    /**
     * Flips the card by changing its representation to a filliped card
     */
    public void flipCard() {
        if (state == CardState.FACE) {
            this.currentTexture = this.cardBack;
            state = CardState.FLIPPED;
        } else if (state == CardState.FLIPPED) {
            this.currentTexture = this.texture;
            state = CardState.FACE;
        }

    }
}
