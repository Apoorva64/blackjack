package base.objects.card;

public enum CardState {
    FACE, FLIPPED, OUT
}
