package base.objects.players;

import base.game.Game;

import static java.lang.Math.random;

public class BotPlayer extends Player {
    public BotPlayer(String name) {
        super(name);
    }

    public void playTurn(Game game) {
        if (random() > 0.5) {
            this.takeCard(game);
        }
        if (random() > 0.5) {
            this.takeCard(game);
        }
    }
}
