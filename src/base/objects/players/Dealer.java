package base.objects.players;

import base.game.Game;

public class Dealer extends Player {
    public Dealer() {
        super("Dealer");
    }

    public void takeCard(Game game) {
        while (this.getPlayerValue() < 16) {
            super.takeCard(game);
        }
    }

    @Override
    public void playTurn(Game game) {

    }

    public void takeSingleCard(Game game) {
        super.takeCard(game);
    }

}


