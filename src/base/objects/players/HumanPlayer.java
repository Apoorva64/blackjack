package base.objects.players;

import base.game.Game;

import javax.swing.*;

public class HumanPlayer extends Player {
    public HumanPlayer(String name) {
        super(name);
    }

    /**
     * Gui to play with the player
     *
     * @param game: game to play on
     */
    public void playTurn(Game game) {
        String[] buttons = {"Yes", "Stop turn"};
        while (true) {
            int returnValue = JOptionPane.showOptionDialog(null, "take card?", this.getName(),
                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.ERROR_MESSAGE, null, buttons, 0);

            if (returnValue == 0) {
                takeCard(game);

            } else if (returnValue == 1) {
                break;
            }
            if (isBusted()) {
                break;
            }
            game.redraw();
        }
        game.redraw();


    }
}
