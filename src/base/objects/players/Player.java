package base.objects.players;

import PaD.PlancheADessin;
import PaD.Texte;
import base.game.Game;
import base.objects.card.Card;
import base.objects.card.CardValue;

import java.awt.*;
import java.util.ArrayList;

import static base.game.PaddingSizes.*;

/**
 * Card game player class
 */
public abstract class Player {
    private final String name;
    private int playerValue;
    private final ArrayList<Card> cards;
    public static final String[] PlayerNames = new String[]{"Liam", "Olivia", "Noah", "Emma", "Oliver", "Ava",
            "Elijah", "Charlotte", "William", "Sophia"};
    private static final int MAX_BANNER_WIDTH = 200;
    private PlayerState state = PlayerState.IDLE;
    private final Texte PlayerNameText;
    private Texte playerStateText;

    protected Player(String name) {
        this.name = name;
        this.cards = new ArrayList<>();
        this.PlayerNameText = new Texte(0, 0, this.name, Game.FONT, Color.BLACK);
        this.playerStateText = new Texte(0, 0, this.state.toString(), Color.BLACK);
    }

    protected Player() {
        this("Player");
    }

    public String getName() {
        return name;
    }

    public int getPlayerValue() {
        return this.playerValue;
    }

    public void setState(PlayerState state) {
        this.state = state;
    }

    /**
     * Check if the sum of the player cards value is more than 21. If it is the player is Busted.
     *
     * @return : the if the status of the player is busted.
     */
    public boolean isBusted() {
        if (this.playerValue > 21) {
            this.state = PlayerState.BUSTED;
            return true;
        }
        return false;
    }

    /**
     * takes a card in a game and updates the player value
     *
     * @param game: game to play on
     */
    public void takeCard(Game game) {
        this.state = PlayerState.PLAYING;
        if (!isBusted()) {
            Card card = game.getLastCard();
            card.flipCard();
            this.cards.add(card);
            if (card.getValue() == CardValue.ACE) {
                if (this.playerValue + CardValue.ACE.getValue() > 21) {
                    this.playerValue += 1;
                    return;
                }
            }
            this.playerValue += card.getValue().getValue();
        }

    }

    public ArrayList<Card> getCards() {
        return this.cards;
    }

    public void clear() {
        this.cards.clear();
    }

    /**
     * Calculates the size on the xy axis of the player banner(cards+ text)
     *
     * @return double[]: returns the height of the player banner
     */
    public double[] getDrawSize() {
        double x = cards.size() * Card.TEXTURE_DIMENSION[0];
        double y = 0;
        y += FONT_HEIGHT.getValue();
        y += Card.TEXTURE_DIMENSION[1];

        return new double[]{x, y};

    }

    /**
     * Draws the player banner(player name+ player cards) on the canvas
     *
     * @param canvas          canvas: canvas to drawCardDeck on
     * @param bannerLocationX :  x position of the start of the drawing
     * @param bannerLocationY :  y position of the start of the drawing
     * @param bannerWidth:    width of the banner
     */
    public void draw(PlancheADessin canvas, double bannerLocationX, double bannerLocationY, double bannerWidth) {

        // Player name
        canvas.supprimer(PlayerNameText);
        PlayerNameText.setOrig(bannerLocationX + (bannerWidth - name.length() * 10) / 2, bannerLocationY);
        canvas.ajouter(this.PlayerNameText);

        //draw Player state
        if (state != PlayerState.PLAYING) {
            canvas.supprimer(playerStateText);
            playerStateText = new Texte(bannerLocationX + (bannerWidth - this.state.toString().length() * 10) / 2, bannerLocationY + Card.TEXTURE_DIMENSION[1] + (double) FONT_HEIGHT.getValue(), this.state.toString(), Color.BLACK);
            canvas.ajouter(playerStateText);
        }

        double cardSpacing;
        if (bannerWidth > MAX_BANNER_WIDTH) {
            cardSpacing = (MAX_BANNER_WIDTH - Card.TEXTURE_DIMENSION[0]) / this.cards.size();

        } else {
            cardSpacing = (bannerWidth - Card.TEXTURE_DIMENSION[0]) / this.cards.size();

        }


        // draw Cards
        for (int x = 0; x < this.cards.size(); x++) {
            Card card = this.cards.get(x);
            double center =  bannerWidth/2-(cardSpacing*(this.cards.size()-1)+Card.TEXTURE_DIMENSION[0])/2;
            card.draw(canvas, bannerLocationX + x * cardSpacing +center , bannerLocationY + FONT_HEIGHT.getValue());
        }

    }


    /**
     * Abstract method to play a turn in the game
     *
     * @param game: game to play on
     */
    public abstract void playTurn(Game game);

    /**
     * flips all the players cards
     */
    public void flipCards() {
        for (Card card : this.cards) {
            card.flipCard();
        }
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                '}';
    }

    /**
     * Resets the player
     */
    public void reset() {
        this.clear();
        this.state = PlayerState.IDLE;
        this.playerValue = 0;

    }
}
