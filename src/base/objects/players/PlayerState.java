package base.objects.players;

/**
 * Represents the player state
 */
public enum PlayerState {
    LOSE, WIN, BUSTED, PLAYING, IDLE
}
